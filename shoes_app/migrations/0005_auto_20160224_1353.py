# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_app', '0004_auto_20160223_2139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='photo1',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo1', blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='photo2',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo2', blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='photo3',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo3', blank=True),
        ),
        migrations.AlterField(
            model_name='producto',
            name='photo4',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo4', blank=True),
        ),
    ]
