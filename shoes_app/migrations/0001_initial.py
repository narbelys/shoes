# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Most_Popular',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('phone', models.IntegerField(default=0)),
                ('amount', models.IntegerField(default=0)),
                ('size', models.IntegerField(default=0)),
                ('country', models.CharField(max_length=200)),
                ('price', models.FloatField(null=True, blank=True)),
                ('email', models.CharField(max_length=1500)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(upload_to=b'photos', verbose_name=b'My Photo')),
                ('name', models.CharField(max_length=200)),
                ('description_short', models.CharField(max_length=1500)),
                ('description', models.CharField(max_length=3000)),
                ('description_adicional', models.CharField(max_length=3000)),
                ('sizes', models.CharField(max_length=1500)),
                ('price', models.FloatField(null=True, blank=True)),
                ('price_before', models.FloatField(null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Producto_home',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('producto', models.ForeignKey(to='shoes_app.Producto')),
            ],
        ),
        migrations.AddField(
            model_name='pedido',
            name='producto',
            field=models.ForeignKey(to='shoes_app.Producto'),
        ),
        migrations.AddField(
            model_name='most_popular',
            name='producto',
            field=models.ForeignKey(to='shoes_app.Producto'),
        ),
    ]
