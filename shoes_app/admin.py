from django.contrib import admin

from .models import Producto, Pedido, Producto_home, Most_Popular, Marca, Precio



class ProductoAdmin(admin.ModelAdmin):
	list_display = ('name', 'description_short', 'admin_image')

class PedidoAdmin(admin.ModelAdmin):
        list_display = ('email', 'price', 'sent', 'producto')

class Producto_homeAdmin(admin.ModelAdmin):
        list_display = ('pk', 'producto')

class Most_PopularAdmin(admin.ModelAdmin):
        list_display = ('pk', 'producto')

class MarcaAdmin(admin.ModelAdmin):
        list_display = ('pk', 'name')

class PrecioAdmin(admin.ModelAdmin):
        list_display = ('price', 'price_before', 'marca', 'producto')




admin.site.register(Producto, ProductoAdmin)
admin.site.register(Pedido, PedidoAdmin)
admin.site.register(Producto_home, Producto_homeAdmin)
admin.site.register(Most_Popular, Most_PopularAdmin)
admin.site.register(Marca, MarcaAdmin)
admin.site.register(Precio, PrecioAdmin)
