# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_app', '0002_auto_20160223_2028'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='sent',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='producto',
            name='status',
            field=models.IntegerField(default=1),
        ),
    ]
