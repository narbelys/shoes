# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='photo',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', verbose_name=b'My Photo'),
        ),
    ]
