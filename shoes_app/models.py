from django.db import models
import os 
from django.conf import settings


class Producto(models.Model):
    photo = models.ImageField(upload_to='shoes_app/shoesstatic/shoes_app/photos', verbose_name='My Photo')
    name = models.CharField(max_length=200)
    description_short = models.CharField(max_length=1500)
    description = models.CharField(max_length=3000)
    description_adicional = models.CharField(max_length=3000)
    sizes = models.CharField(max_length=1500)
    price = models.FloatField(null=True, blank=True)
    price_before = models.FloatField(null=True, blank=True)
    status = models.IntegerField(default=1)
    date = models.DateTimeField(auto_now_add=True)
    categoria = models.IntegerField(default=1)
    
    photo1 = models.ImageField(upload_to='shoes_app/static/shoes_app/photos', verbose_name='My Photo1', null=True, blank=True)
    photo2 = models.ImageField(upload_to='shoes_app/static/shoes_app/photos', verbose_name='My Photo2', null=True, blank=True)
    photo3 = models.ImageField(upload_to='shoes_app/static/shoes_app/photos', verbose_name='My Photo3', null=True, blank=True)
    photo4 = models.ImageField(upload_to='shoes_app/static/shoes_app/photos', verbose_name='My Photo4', null=True, blank=True)
    
    def __unicode__(self):
        return self.name
	
    def filename(self):
        return os.path.basename(self.photo.name)
    def filename1(self):
        return os.path.basename(self.photo1.name)
    def filename2(self):
        return os.path.basename(self.photo2.name)
    def filename3(self):
        return os.path.basename(self.photo3.name)
    def filename4(self):
        return os.path.basename(self.photo4.name)
    
    def screenshots_as_list(self):
        return self.sizes.split(',')
    
    def get_price(self):
        return Precio.objects.filter(producto=self)
    
    def admin_image(self):
        return '<img src="' + settings.STATIC_ADMIN +'/shoes_app/photos/%s" style="width: 80px;"/>' % os.path.basename(self.photo.name)
    admin_image.allow_tags = True
    
    
class Marca(models.Model):
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name

class Precio(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    price = models.FloatField(null=True, blank=True)
    price_before = models.FloatField(null=True, blank=True)
    marca = models.ForeignKey(Marca, on_delete=models.CASCADE)
    


class Pedido(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    phone = models.IntegerField(default=0)
    amount = models.IntegerField(default=0)
    size = models.IntegerField(default=0)
    country = models.CharField(max_length=200)
    price = models.FloatField(null=True, blank=True)
    price_marca = models.ForeignKey(Precio, on_delete=models.CASCADE, default=0, null=True)
    email = models.CharField(max_length=1500)
    sent = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)
    
    
class Producto_home(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    
class Most_Popular(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)



