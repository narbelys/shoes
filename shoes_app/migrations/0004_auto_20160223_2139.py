# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_app', '0003_auto_20160223_2126'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='photo1',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo1'),
        ),
        migrations.AddField(
            model_name='producto',
            name='photo2',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo2'),
        ),
        migrations.AddField(
            model_name='producto',
            name='photo3',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo3'),
        ),
        migrations.AddField(
            model_name='producto',
            name='photo4',
            field=models.ImageField(upload_to=b'shoes_app/static/shoes_app/photos', null=True, verbose_name=b'My Photo4'),
        ),
    ]
