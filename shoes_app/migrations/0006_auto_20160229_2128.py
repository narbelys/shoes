# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_app', '0005_auto_20160224_1353'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Precio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('price', models.FloatField(null=True, blank=True)),
                ('price_before', models.FloatField(null=True, blank=True)),
                ('marca', models.ForeignKey(to='shoes_app.Marca')),
            ],
        ),
        migrations.AlterField(
            model_name='producto',
            name='photo',
            field=models.ImageField(upload_to=b'shoes_app/shoesstatic/shoes_app/photos', verbose_name=b'My Photo'),
        ),
        migrations.AddField(
            model_name='precio',
            name='producto',
            field=models.ForeignKey(to='shoes_app.Producto'),
        ),
        migrations.AddField(
            model_name='pedido',
            name='price_marca',
            field=models.ForeignKey(default=0, to='shoes_app.Precio', null=True),
        ),
    ]
