from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^detalle/(?P<detalle_id>[0-9]+)$', views.detalle, name='detalle'),
    url(r'^allmean/$', views.allProductmean, name='allProductmean'),
    url(r'^allwoman/$', views.allProductwoman, name='allProductwoman'),
    url(r'^admin/', admin.site.urls),
    url(r'^paint/$', views.paint, name='paint'),
    url(r'^cambio/$', views.cambio, name='cambio'),
    url(r'^contacto/$', views.contacto, name='contacto'),
]
