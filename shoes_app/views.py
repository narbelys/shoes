# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Producto_home, Producto, Most_Popular, Pedido, Marca, Precio

from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
        
from django.core.mail import send_mail
from django.conf import settings

# Create your views here.

def index(request):
    producto_home = Producto_home.objects.all()
    print producto_home
    template_name = 'shoes_app/index.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    return render_to_response(
		template_name, 
		{ 'producto_home': producto_home, 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)


def detalle(request, detalle_id):
    product = Producto.objects.get(pk=detalle_id)
    prices = Precio.objects.filter(producto=product)
    price_active = Precio.objects.get(producto=product, marca__name="Generica")
    most_popular = Most_Popular.objects.all()
    template_name = 'shoes_app/single.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    
    if request.method == 'GET':
        return render_to_response(
            template_name, 
            { 'product': product, 'prices':prices, 'price_active':price_active, 'most_popular': most_popular, 'url':url, 'static_admin':static_admin
            }, 
            context_instance=RequestContext(request)
        )
    elif request.method == 'POST':
        print 'POST'
        id_product = request.POST.get('id_product',None)
        size_input = request.POST.get('size_input',None)
        marca_input = request.POST.get('marca_input',None)
        qty = request.POST.get('qty',None)
        despacho = request.POST.get('despacho',None)
        email = request.POST.get('email',None)
        
        print qty
        producto = Producto.objects.get(pk=id_product)
        print producto
        precio_marca = Precio.objects.get(marca__name=marca_input, producto=producto)
        print precio_marca
        pedido = Pedido(producto=producto, size=size_input, amount=qty, country=despacho, email=email, price=precio_marca.price, price_marca=precio_marca)
        print pedido
        #pedido = Pedido(producto=producto, size=size_input, amount=qty, country=despacho, email=email, price=5654654)
        pedido.save()
        print "salvado"
        try:
            send_mail('Pedido zapatos', 'Realizaron un pedido correo: '+ email, 'shoes@example.com',['narbelys@gmail.com'], fail_silently=False)
        except: 
            pass
        
        return render_to_response(
            template_name, 
            { 'product': product, 'most_popular': most_popular, 'success': 'Su pedido se ha realizado con éxito en breves momento recibirá un email para completar los detalles de su compra.', 'url':url, 'static_admin':static_admin
            }, 
            context_instance=RequestContext(request)
        )
        


def allProductmean(request):
    products = Producto.objects.filter(status=1, categoria=2)
    most_popular = Most_Popular.objects.all()
    template_name = 'shoes_app/men.html'
    #template_name = 'shoes_app/detalle.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    
    return render_to_response(
		template_name, 
		{ 'products': products, 'most_popular': most_popular, 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)

def allProductwoman(request):
    products = Producto.objects.filter(status=1, categoria=1)
    most_popular = Most_Popular.objects.all()
    template_name = 'shoes_app/woman.html'
    #template_name = 'shoes_app/detalle.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    
    return render_to_response(
		template_name, 
		{ 'products': products, 'most_popular': most_popular, 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)

def paint(request):
    products = Producto.objects.all()
    most_popular = Most_Popular.objects.all()
    template_name = 'shoes_app/paint.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    
    return render_to_response(
		template_name, 
		{ 'products': products, 'most_popular': most_popular, 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)

def cambio(request):
    template_name = 'shoes_app/cambio.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    return render_to_response(
		template_name, 
		{ 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)

def contacto(request):
    template_name = 'shoes_app/contacto.html'
    url = settings.FORCE_SCRIPT_NAME
    static_admin = settings.STATIC_ADMIN
    return render_to_response(
		template_name, 
		{ 'url':url, 'static_admin':static_admin
		}, 
		context_instance=RequestContext(request)
	)


